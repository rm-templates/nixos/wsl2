# NIXOS CONFIG

A nixos config for enviroments where you might just be unable to use a regular NixOS ;) It is also set up to use a proxy server

## Installation

### Set up blank nixos
First download and import the tarball from [github.com/nix-community/NixOS-WSL](https://github.com/nix-community/NixOS-WSL). In my case I keep my source files in C:\wsl\source and the distros images in C:\wsl\distros but you can handle this as you like. Just rename the folders accordingly in the following examples

```powershell
wsl --import nixos ./distros/nixos ./source/tarball.tar.gz
```

### Px Proxy
To install you need a running proxy instance. This setup expects your windows machine to have running a [px proxy](https://github.com/genotrance/px) by default on the ip 169.254.254.1:3128. For your initial set up please make sure px proxy is up and running on your windos machine. You can use this px.ini as a starting point

```ini
[proxy]
listen = 127.0.0.1
port = 3128
gateway = 0
hostonly = 0
allow = 127.0.0.1,169.254.254.*
noproxy = 169.254.254.*

[settings]
workers = 4
threads = 10
idle = 30
socktimeout = 20.0
proxyreload = 60
foreground = 1
log = 0
```

### Windows network interface

This instance will set your wsl to point to a proxy on 169.254.254.1:3128 by default. In order for this to work you need to be able to reach your host machines WSL interface under that address. This will be done by the setup_host_network.ps1 script. The easiest is to let the create_shortcut.ps1 script run which will create that for you on your desktop. Just run this in powershell.

(Please make sure you run this from your windows file system. Pointing to a \\wsl$ path at least for me resulted in a report by windows about the script not being digitally signed)
```powershell
powershell.exe -File scripts_host/create_shortcut.ps1
```

now run the shortcut and your windows networks should be ready to go.

### Install repo

1. Copy the variables template in the nixos folders and update according with your info
```sh
cp template_variables.nix nixos/variables.nix
```

2. Copy the contents over to /etc/nixos manually or use the script for that
```sh
sudo ./scripts/apply.sh
```

3. Rebuild the system (you only need the env vars for the proxy on the very first go after that the proxy is set from within the config files"
```sh
sudo http_proxy="169.254.254.1:3128" https_proxy="169.254.254.1:3128" HTTP_PROXY="169.254.254.1:3128" HTTPS_PROXY="169.254.254.1:3128" nixos-rebuild switch
```
