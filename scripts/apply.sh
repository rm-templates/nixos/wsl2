#!/usr/bin/env bash

BASEDIR=$(dirname $0)

while getopts i:f:h flag
do
    case "${flag}" in
        i) ip=${OPTARG};;
        f) folder=${OPTARG};;
        h) help=true;;
    esac
done

if [ "${ip}" != "" ]; then
  echo "Your ip is ${ip}";
fi

if [ $help ]; then
  echo """
    Usage

    -i     if you want to apply your config to a different IP
    -f     set the folder you want to copy to (defaults to /etc/nixos)
    -h     Display this help
  """

  exit 0;
fi

if [[ "$folder" == "" ]]; then
  folder="/etc/nixos"
fi

if [[ $EUID -ne 0 && "${folder}" == "/etc/nixos" ]]; then
   echo "This script must be run as root" 
   exit 1
fi


echo -n "This will overwrite existing files in /etc/nixos. Are you sure you want to proceeed [y/n]: "
read REPLY

if [[ "$REPLY" == "y" ]]; then

  if [[ "$ip" != "" ]]; then
    echo applying to remote host folder ${folder}
    
    echo not yet implemented
    exit 1

  else
    echo "applying configuration to local folder ${folder}"
    rm -r ${folder}/*
    cp -r $BASEDIR/../nixos/* ${folder}
    echo "done"
  fi
fi
