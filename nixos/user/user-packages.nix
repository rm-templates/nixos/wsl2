{ config, pkgs, vars, ...}:
{

  

  users.users.${vars.main_user} = {

    packages = with pkgs; [
      # cypress
      argocd
      dos2unix
      drawio
      git-crypt
      gnupg
      kate
      kubecolor
      kubectl
      kubernetes-helm
      kubevirt
      nodejs_20
      openssl
      packer
      skaffold
      sops
      tigervnc
    ];
  };

  # ADDTIONS
  
  ## logseq
  ### Currently necessary until logseq is updated to new electron. Remove when updated
  nixpkgs.config.permittedInsecurePackages = [
    # Logseq
    "electron-24.8.6"
    "electron-25.9.0"

    # Home Assistant
    # "openssl-1.1.1w"
  ];
}
