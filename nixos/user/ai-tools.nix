{ pkgs, config, vars, ...}:
let
  api_key = (import ../credentials/_.nix).openAI;
in
{
  environment.variables = {
    OPENAI_API_KEY=api_key;
  };

  users.users.${vars.main_user} = {
    packages = with pkgs; [
      shell_gpt
      chatgpt-cli
    ];
  };
}