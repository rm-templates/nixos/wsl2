{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
      cloud-utils
      dig
      inetutils
      minikube
      ncdu
      nix-prefetch-github
      nmap
      pciutils
      qemu
      ripgrep
      tree
      xclip
      xorriso
      yq-go
  ];
}
