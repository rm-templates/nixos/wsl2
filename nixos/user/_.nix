{ lib, config, home-manager, vars, ... }:
with lib;
with types;
{

  imports = [
    ./coding-rust.nix
    ./direnv.nix
#    ./flatpak.nix
    ./system-packages.nix
    ./user-packages.nix
#    ./ai-tools.nix
  ];
}
