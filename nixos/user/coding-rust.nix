{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    rustc
    cargo
    gcc
    lldb_17
  ];
}
