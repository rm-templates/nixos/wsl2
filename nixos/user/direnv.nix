{ config, pkgs, vars, ...}:
{

  users.users.${vars.main_user} = {
    packages = with pkgs; [
      direnv
    ];
  };

  environment.interactiveShellInit = ''
    eval "$(direnv hook bash)"
  '';
}