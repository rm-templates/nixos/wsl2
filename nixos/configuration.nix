{ config, pkgs, vars, ... }:
let
  proxy = vars.proxy;
  german = "de_DE.UTF-8";
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  system.stateVersion = "24.05";
  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n.defaultLocale = "de_DE.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = german;
    LC_IDENTIFICATION = german;
    LC_MEASUREMENT = german;
    LC_MONETARY = german;
    LC_NAME = german;
#    LC_NUMERIC = "de_DE.UTF-8";
    LC_PAPER = german;
    LC_TELEPHONE = german;
#    LC_TIME = "de_DE.UTF-8";
  };

  # ADDITIONS
  programs.nix-ld.enable = true;

  environment.systemPackages = with pkgs; [
    wget
    nmap
    inetutils
    yq-go
  ];

  environment.sessionVariables = {
    DONT_PROMPT_WSL_INSTALL="true";
  };

  users.users.${vars.main_user} = {
    isNormalUser = true;
    description = "${vars.main_user}";
    extraGroups = [ "wheel" ];
  };

  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = [
      "nix-2.16.2"
    ];
  };



  # services.vscode-server.enable = true;

  wsl.extraBin = with pkgs; [
    { src = "${coreutils}/bin/uname"; }
    { src = "${coreutils}/bin/dirname"; }
    { src = "${coreutils}/bin/readlink"; }
    { src = "${coreutils}/bin/cut"; }
    { src = "${coreutils}/bin/nix"; }
    { src = "${coreutils}/bin/rm"; }
  ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
}
