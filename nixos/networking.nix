{ pkgs, vars, lib, ...}:
let
  proxy = vars.proxy;
  certFiles = 
    if builtins.pathExists ./certs then
      builtins.attrNames (builtins.readDir ./certs )
    else
      [];
  certs = builtins.filter (f: builtins.match ".*\.pem" f != null) certFiles;
  certContents = map( v: builtins.readFile (lib.path.append ./certs "${v}")) certs;
in
{   
  networking = {
#    networkmanager.enable = true;
#
#    users.users.${vars.main_user} = {
#      extraGroups = [ "networkmanager" ];
#    };

    proxy = {
      httpsProxy  = "http://${proxy.ip}:${proxy.port}";
      httpProxy   = "http://${proxy.ip}:${proxy.port}";
      noProxy = proxy.no_proxy;
    };

    nameservers = [ vars.networking.default_nameserver ];

    interfaces.eth0 = {
      ipv4.addresses = [{
        address = vars.networking.wsl_static_ip;
        prefixLength = 24;
      }];
    };

    defaultGateway = vars.networking.wsl_host_ip;

    
  };

  security.pki.certificates = [
  ] ++ certContents;

}
