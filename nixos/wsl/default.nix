{ pkgs, vars, ... }:
{
  wsl = {
    enable = true;
    defaultUser = vars.main_user;
    nativeSystemd = true;
    interop.register = true;
    
    wslConf.interop.enabled = true;
    wslConf.user.default = vars.main_user;
    wslConf.network = {
      generateHosts = false;
      generateResolvConf = false;
    };
  };
}
