{ pkgs, config, ...}:
{

  # requires .programs.bash to be enabled in home-manager
  programs.powerline-go = {
    enable = false;
    modules = [
      "cwd"
      "git"
      "node"
      "docker"
      "kube"
      "root"
    ];

    newline = true;

    settings = {
      cwd-mode = "dironly";
      theme = "solarized";
    };
  };

}
