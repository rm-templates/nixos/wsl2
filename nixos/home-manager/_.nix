{ config, pkgs, home-manager, lib, vars, nixvim, ... }:
{
  imports = [
    ./git.nix
    ./bash.nix
    ./nixvim
    ./vscode.nix
    ./starship.nix
  ];
    home.stateVersion = "${vars.release}";
}
