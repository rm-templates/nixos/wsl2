{ config, pkgs, lib, vars, ... }:
let
  userName = vars.git.username;
  userMail = vars.git.email;
in
{

  programs.git = {
    enable = true;
    userEmail = "${userMail}";
    userName = "${userName}";

    extraConfig = {
      init = {
        defaultBranch = "main";
      };
    };
  };
}
