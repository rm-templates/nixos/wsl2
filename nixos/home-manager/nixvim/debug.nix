{ pkgs, ... }: 
let 
  lldb = pkgs.lldb_17;
in
{

  programs.nixvim = {
    plugins.dap = {
      enable = true;
      extensions = {
        dap-ui.enable = true;
        dap-virtual-text.enable = true;
      };
    };

    plugins.packer = {
      enable = true;
      plugins = [];
    };

    plugins.rust-tools.enable = true;

    extraPlugins = with pkgs.vimPlugins; [
      nvim-gdb
    ];

    keymaps = [      
      # Dap
      { mode = "n"; action = "<cmd>DapToggleBreakpoint<cr>";  key = "<leader>b"; }
      { mode = "n"; action = "<cmd>DapContinue<cr>"; key = "<F5>"; }
      { mode = "n"; action = "<cmd>DapStepOver<cr>"; key = "<F10>"; }
      { mode = "n"; action = "<cmd>DapStepInto<cr>"; key = "<F11>"; }
      { mode = "n"; action = "<cmd>DapStepOut<cr>"; key = "<F12>"; }
      { mode = "n"; action = "<cmd>lua function() require 'dap'.set_breakpooint(vim.fn.input('Breakpoint condition: '))<cr>"; key = "<leader>B"; }

      # Dap-ui
      { mode = "n"; action = "<cmd>lua require(\"dapui\").toggle()<cr>"; key = "<leader>d"; }

      # LSP
      { mode = "n"; action = "<cmd>lua vim.lsp.buf.rename()<cr>"; key="<F2>"; }
      { mode = "n"; action = "<cmd>lua vim.lsp.buf.hover()<cr>"; key="<leader>h"; }

      { mode = "n"; action = "<cmd>Telescope live_grep<cr>"; key="<leader>lg"; }
    ];


    extraConfigLua = ''
      local dap, dapui = require("dap"), require("dapui")
      local rt = require("rust-tools")

      -- DEBUG LISTENERS
      dap.listeners.before.attach.dapui_config = function()
        dapui.open()
      end
      dap.listeners.before.launch.dapui_config = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated.dapui_config = function()
        dapui.close()
      end
      dap.listeners.before.event_exited.dapui_config = function()
        dapui.close()
      end

      dap.set_log_level('DEBUG')

      -- DEBUG RUST
      dap.adapters.lldb = {
        type = 'executable',
        command = '${lldb}/bin/lldb-vscode', -- adjust as needed, must be absolute path
        name = 'lldb'
      }

      dap.adapters.gdb = {
        type = "executable",
        command = "gdb",
        args = { "-i", "dap" }
      }

      -- DEBUG CONFIG RUST
      dap.configurations.rust = {
        {
          name = 'Launch',
          type = 'lldb',
          request = 'launch',
          program = function()
            return vim.fn.input('Path of the executable: ', vim.fn.getcwd() .. '/', 'file')
          end,
          cwd = "''${workspaceFolder}",
          stopOnEntry = false,
          args = {},
        },
      }

      -- DEBUG SETUP RUST TOOLS
      rt.setup({
        server = {
          on_attach = function(_, bufnr)
            vim.keymap.set("n", "<leader>H", rt.hover_actions.hover_actions, { buffer = bufnr })
            vim.keymap.set("n", "<leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
          end,
        },
      })
    '';
  };


}
