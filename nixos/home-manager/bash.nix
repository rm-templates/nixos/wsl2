{ config, pkgs, ...}:
{
  programs.bash = {
    enable = true;

    initExtra = ''
      alias la="ls -lha --color=auto"

      command -v kubecolor >/dev/null 2>&1 && alias kubectl="kubecolor"
      source <(kubectl completion bash)      
      alias k=kubectl
      alias kns="kubectl config set-context --current --namespace "
      complete -F __start_kubectl k
    '';
    
  };
}