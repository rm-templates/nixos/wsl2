{
  description = "WSL build";

  inputs = {
    # The most widely used is `github:owner/name/reference`,
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
    
    # home-manager, used for managing user configuration
    home-manager = {
      url = "github:nix-community/home-manager/7403ed49801e1a30ed0ffdec5b60c17f0cbf7bd5";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-wsl = {
      url = "github:nix-community/NixOS-WSL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, ... }@inputs: {
    nixosConfigurations = {
      "nixos" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";

        specialArgs = {
          home-manager = inputs.home-manager;
          lib = nixpkgs.lib;
          vars = import ./variables.nix;
          # credentials = import ./credentials/_.nix;
          nixvim = inputs.nixvim;
        };
        
        modules = [
          inputs.nixos-wsl.nixosModules.wsl
          ./configuration.nix
          ./networking.nix

          ./services/_.nix
          ./user/_.nix
          ./appearance
          ./wsl

          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.chris = (import ./home-manager/_.nix);

            home-manager.extraSpecialArgs = {
              vars = import ./variables.nix;
              # credentials = import ./credentials/_.nix;
              nixvim = inputs.nixvim;
            };
          }
        ];
      };
    };
  };
}
