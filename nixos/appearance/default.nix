{ config, pkgs, ...}:
{
  fonts.packages = with pkgs; [
    fira-code
    fira-code-symbols
    hasklig
    nerdfonts
  ];
}
