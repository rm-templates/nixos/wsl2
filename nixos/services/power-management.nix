{ config, pkgs, ... }:
{
  services.thermald.enable = true;
  # services.auto-cpufreq.enable = true;

  powerManagement = {
    powertop.enable = false;
    # cpuFreqGovernor = "powersave";
  };

  services.power-profiles-daemon = {
    enable = false;
  };

  boot.kernelParams = [
    "acpi_enforce_resources=lax"
  ];

  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
      CPU_SCALING_GOVERNOR_ON_BAT = "powersafe";

      CPU_MIN_PERF_ON_AC = 20;
      CPU_MAX_PERF_ON_AC = 100;
      CPU_MIN_PERF_ON_BAT = 0;
      CPU_MAX_PERF_ON_BAT = 35;

      # Seems like cpu boost is causing major temp rise and throttling
      CPU_BOOST_ON_AC = 0;
      CPU_BOOST_ON_BAT = 0;

      CPU_HWP_DYN_BOOST_ON_AC = 1;
      CPU_HWP_DYN_BOOST_ON_BAT = 0;

      MEM_SLEEP_ON_AC = "s2idle";
      MEM_SLEEP_ON_BAT= "deep";
    };
  };
}