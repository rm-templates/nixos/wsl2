{ config, pkgs, ... }:
{
  imports = [
    ./docker-registry.nix
    ./docker.nix
    ./systemd.px-proxy.nix
    # ./kvm.nix
    # ./power-management.nix
  ];
}
