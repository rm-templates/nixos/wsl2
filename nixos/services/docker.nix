{ config, pkgs, vars, ... }:
{
  users.users.${vars.main_user}.extraGroups = [ "docker" ];

  virtualisation = {
    docker = {
      enable = true;
      
      rootless = {
        setSocketVariable = true;
      };

      rootless.daemon.settings = {
        insecure-registries = [ "127.0.0.1:5000" ];
      };

      daemon.settings = {
        insecure-registries = [ "127.0.0.1:5000" ];
      };
    };    
  };
}
