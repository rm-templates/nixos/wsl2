{ config, pkgs, vars, ...}:
{
  virtualisation.libvirtd = {
    enable = true;
  };

  programs.virt-manager = {
    enable = true;
  };

  users.users.${vars.main_user}.extraGroups = [
    "libvirtd"
  ];
}