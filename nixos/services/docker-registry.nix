{ config, pkgs, ... }:
{
  services.dockerRegistry = {
    enable = true;
    port = 32000;
  };
}
