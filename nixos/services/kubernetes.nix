{ config, pkgs, ...}:
let
  kubeMasterIP = "127.0.0.1";
  kubeMasterHostname = "api.kube";
  kubeMasterAPIServerPort = 6443;
in
{
  # resolve master hostname
  networking.extraHosts = "${kubeMasterIP} ${kubeMasterHostname}";

    # packages for administration tasks
  environment.systemPackages = with pkgs; [
    kubernetes
  ];

  boot.kernelModules = [ "ceph" ];


  services.kubernetes = let 
    api = "https://${kubeMasterHostname}:${toString kubeMasterAPIServerPort}";
  in 
  {
    roles = ["master" "node"];
    masterAddress = kubeMasterHostname;
    apiserverAddress = api;
    # easyCerts = false;
    
    apiserver = {
      securePort = kubeMasterAPIServerPort;
      advertiseAddress = kubeMasterIP;
      allowPrivileged = true;
    };

    # use coredns
    addons = {
      dns.enable = true;
    };

    # needed if you use swap
    kubelet = {
      extraOpts = "--root-dir=/var/lib/kubelet --fail-swap-on=false";
      kubeconfig.server = api;
    };
  };

}