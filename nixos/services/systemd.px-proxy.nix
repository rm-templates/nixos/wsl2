{  vars, ...}:
{
  systemd.services.px = {
    description = "px proxy execution";
    after = [ "network.target" ];
    requires = [ "network.target" ];

    serviceConfig = {
      Restart = "always";
      ExecStart = "/mnt/c/Windows/System32/cmd.exe  ${vars.proxy.px_proxy_location} --foreground=0";
      ExecStop = "/mnt/c/Windows/System32/cmd.exe ${vars.proxy.px_proxy_location} --quit";
    };

    wantedBy = [ "multi-user.target" ];
  };
} 
