{
  main_user = "relief-melone";
  release = "23.11";
  proxy = {
    ip = "169.254.254.1";
    port = "3128";
    no_proxy = "127.0.0.1";
    px_proxy_location = "/mnt/c/__local/portable_programs/px_proxy/px.exe";
  };
  laptop = {
    make = "undefined";
    model = "undefined";
  };
  networking = {
    default_nameserver = "169.254.254.1";
    wsl_host_ip = "169.254.254.1";
    wsl_static_ip = "169.254.254.2";
  };
  git = {
    username = "Relief Melone";
    email = "john.doe@gmail.com";
  };

}
