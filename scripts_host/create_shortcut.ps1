Try
{
    $ShortcutLocation = "$ENV:USERPROFILE\Desktop\wsl2_network_setup.lnk"
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($ShortcutLocation)
    $Shortcut.TargetPath = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
    $Shortcut.Arguments = """$PSScriptRoot\setup_host_network.ps1"""
    $Shortcut.WorkingDirectory = $PSScriptRoot
    $Shortcut.IconLocation = "$PSScriptRoot\media\tux.ico,0"
    $Shortcut.Save()

    $bytes = [System.IO.File]::ReadAllBytes($ShortcutLocation)
    $bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
    [System.IO.File]::WriteAllBytes($ShortcutLocation, $bytes)
}
Catch
{
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName

    Write-Host "An error occurred: $ErrorMessage"
    Write-Host $_.ScriptStackTrace
    Break
}
