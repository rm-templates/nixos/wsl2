$wsl_host="169.254.254.1";

echo "Making sure WSL is running"
wsl --system -e echo "WSL up..."
echo "===Setting up Windows machines network for WSL===";
echo "assigning static ip address...";
netsh interface ip add address "vEthernet (WSL)" $wsl_host 255.255.255.0;
Start-Sleep 5;
echo "setting up port forwarding..."
netsh interface portproxy reset;
Start-Sleep 5;
netsh interface portproxy add v4tov4 listenaddress=$wsl_host listenport=3128 connectaddress=127.0.0.1 connectport=3128;

echo "Window will close automatically in 5 seconds"
Start-Sleep 5;
